# gitlab-ci-pulumi-microk8s

> :warning: **This is not ready for testing yet, it should be ready before the Bend DevOps Meeting July 18, 2023**

## Getting started

To use this project, you'll need to have the following tasks completed:

[ ] Fork this repository on GitLab

[ ] Install 64bit Raspbian on a Raspberry Pi. 4 is recommended with at least 4 gb of RAM. [Link](https://www.makeuseof.com/install-64-bit-version-of-raspberry-pi-os/)

[ ] Install microk8s [Link](https://microk8s.io/docs/install-raspberry-pi)

[ ] Install the GitLab agent into your microk8s installation [Link](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#register-the-agent-with-gitlab)

[ ] Install a GitLab runner [Link](https://docs.gitlab.com/runner/install/kubernetes-agent.html)

[ ] Configure your GitLab Project to only use your runner [Link](https://docs.gitlab.com/ee/ci/runners/runners_scope.html)

