import * as k8s from "@pulumi/kubernetes";
import * as pulumi from "@pulumi/pulumi";

console.log(`Commit: ${process.env.CI_COMMIT_SHORT_SHA}`);

// Create the k8s resources.

const appLabels = { app: "gitlab-ci-pulumi-microk8s" };

const namespace = new k8s.core.v1.Namespace(
  "gitlab-ci-pulumi-microk8s-namespace",
  {
    metadata: {
      name: "svelte-app",
    },
  }
);

const replicaSet = new k8s.apps.v1.ReplicaSet(
  "gitlab-ci-pulumi-microk8s-replicaset",
  {
    metadata: {
      labels: appLabels,
      namespace: namespace.metadata.name,
    },
    spec: {
      replicas: 5,
      selector: { matchLabels: appLabels },
      template: {
        metadata: { labels: appLabels, namespace: namespace.metadata.name },
        spec: {
          containers: [
            {
              name: "gitlab-ci-pulumi-microk8s",
              image: `${process.env.CI_REGISTRY_IMAGE}:${process.env.CI_COMMIT_SHORT_SHA}`,
              ports: [{ name: "http", containerPort: 3000 }],
            },
          ],
        },
      },
    },
  },
  {
    replaceOnChanges: ["spec.template.spec.containers"],
  }
);

const service = new k8s.core.v1.Service("gitlab-ci-pulumi-microk8s-service", {
  metadata: {
    labels: replicaSet.spec.template.metadata.labels,
    namespace: namespace.metadata.name,
  },
  spec: {
    type: "NodePort",
    ports: [{ port: 80, targetPort: "http" }],
    selector: appLabels,
  },
});

// Create the ingress

let serviceUrl: pulumi.Output<string> = pulumi.output("");

pulumi
  .all([service.metadata.name, namespace])
  .apply(([serviceName, namespace]) => {
    const ingress = new k8s.networking.v1.Ingress(
      "gitlab-ci-pulumi-microk8s-ingress",
      {
        metadata: {
          labels: replicaSet.spec.template.metadata.labels,
          namespace: namespace.metadata.name,
        },
        spec: {
          rules: [
            {
              // host: "gitlab-ci-pulumi-microk8s",

              http: {
                paths: [
                  {
                    path: "/",
                    pathType: "Prefix",
                    backend: {
                      service: {
                        name: serviceName,
                        port: {
                          number: 80,
                        },
                      },
                    },
                  },
                ],
              },
            },
          ],
        },
      }
    );

    const serviceHostname = ingress.spec.rules[0].host;
    serviceUrl = pulumi.interpolate`http://${serviceHostname}:${service.spec.ports[0].port}`;
    pulumi.all([serviceUrl, ingress]).apply(([serviceUrl, ingress]) => {
      console.log(serviceUrl);
    });
  });

// Export the service's IP address and the URL for the load balanced application.
